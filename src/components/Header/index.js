import React, {Component} from 'react'
import {Menu, Backdrop} from '../'
import './styles.scss'

class Header extends Component {
    constructor(props) {
        super(props)

        this.state = {
            active: false,
        }
    }

    toggleActive = () => {
        this.setState({
            active: !this.state.active
        })
    }

    render () {
        return (
            <div className="page-header">
                <header className="main-header container">
                    <div className="logo">
                        iaMMast
                    </div>

                    <Menu active={this.state.active}
                          toggleActive={this.toggleActive} />
                </header>

                <Backdrop active={this.state.active}
                          toggleActive={this.toggleActive}
                          showButton={true} />
            </div>
        )
    }
}

export default Header