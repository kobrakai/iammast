import React, {Component} from 'react'
import {Motion, spring} from 'react-motion'
import "./styles.scss";

class Backdrop extends Component {
    render () {
        return (
            <Motion style={{
                currentPosition: spring(this.props.active ? 20 : -50, 
                    { stiffness: 140, damping: 20 }
                ),
                currentOpacity: spring(this.props.active ? 1 : 0, 
                    { stiffness: 140, damping: 20 }
                )
            }}>
                {({currentOpacity, currentPosition}) =>
                    <div className="backdrop" style={{opacity: currentOpacity}}>
                        {this.props.showButton > 0 &&
                            <div className="backdrop__button button-round"
                                 onClick={this.props.toggleActive}
                                 style={{bottom: `${currentPosition / 10}rem`}}>
                                X
                            </div>
                        }
                    </div>
                }
            </Motion>
        )
    }
}

export default Backdrop