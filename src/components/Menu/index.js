import React, {Component} from 'react'
import {Motion, spring} from 'react-motion'
import './styles.scss'

class Menu extends Component {
    constructor(props) {
        super(props)

        this.state = {
            button: this.refs.menuButton
        }
    }

    componentDidMount() {
        this.setState({ button: this.refs.menuButton })
        window.addEventListener('resize', this.updateButton);
    }

    updateButton = () => {
        this.setState({
            button: this.refs.menuButton
        })
    }

    render () {
        let button = this.state.button ? this.state.button.getBoundingClientRect() : false,
            height = button ? button.height / 2 / 10 : 0;
            
        return (
            <div className={'menu-button' + (this.props.active ? ' active' : '')}
                 onClick={this.props.toggleActive}>
                <span ref="menuButton" className="button-round">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                        <g fill="white">
                            <path d="M491.3 235.3H20.7C9.3 235.3 0 244.6 0 256s9.3 20.7 20.7 20.7h470.6c11.4 0 20.7-9.3 20.7-20.7C512 244.6 502.7 235.3 491.3 235.3z"/>
                            <path d="M491.3 78.4H20.7C9.3 78.4 0 87.7 0 99.1c0 11.4 9.3 20.7 20.7 20.7h470.6c11.4 0 20.7-9.3 20.7-20.7C512 87.7 502.7 78.4 491.3 78.4z"/>
                            <path d="M491.3 392.2H20.7C9.3 392.2 0 401.5 0 412.9s9.3 20.7 20.7 20.7h470.6c11.4 0 20.7-9.3 20.7-20.7S502.7 392.2 491.3 392.2z"/>
                        </g>
                    </svg>
                </span>
                <Motion style={{
                    scale: spring(this.props.active ? .8 : 0, 
                        { stiffness: 140, damping: 20 }
                    ),
                    radius: spring(this.props.active ? 12 : height, 
                        { stiffness: 140, damping: 20 }
                    )
                }}>
                {({scale, radius}) =>
                    <nav className="menu" style={{
                            borderRadius: `${height}rem ${height}rem ${height}rem ${radius}rem`,
                            width: `${scale * 100}vw`,
                            height: `${scale * 100}vh`,
                            minHeight: `${button.height / 10}rem`,
                            minWidth: `${button.height / 10}rem`
                        }}>
                        <div className="menu__list">
                            <ul>
                                <li>
                                    <a href="#banner-section" onClick={this.props.menuToggle}>
                                        Banner
                                    </a>
                                </li>
                                <li>
                                    <a href="#about-section" onClick={this.props.menuToggle}>
                                        About
                                    </a>
                                </li>
                                <li>
                                    <a href="#portfolio-section" onClick={this.props.menuToggle}>
                                        Portfolio
                                    </a>
                                </li>
                                <li>
                                    <a href="#contact-section" onClick={this.props.menuToggle}>
                                        Contact
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                }
                </Motion>
            </div>
        )
    }
}

export default Menu