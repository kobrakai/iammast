import Header from './Header'
import Footer from './Footer'
import Menu from './Menu'
import Banner from './Banner'
import Contact from './Contact'
import About from './About'
import Portfolio from './Portfolio'
import Backdrop from './Backdrop'

export {
    Header,
    Footer,
    Menu,
    Banner,
    Contact,
    About,
    Portfolio,
    Backdrop
}