import React from 'react'
import { storiesOf } from '@storybook/react'
import '../containers/App/styles.scss'

import {
  Header,
  Menu
} from '../components'

storiesOf('Header', module)
  .add('Normal', () => (
    <Header />
  ))

storiesOf('Menu', module)
  .add('Closed', () => (
    <Menu active={false} />
  ))
  .add('Opened', () => (
    <Menu active={true} />
  ))
