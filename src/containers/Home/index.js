import React, {Component} from 'react'
import './styles.scss'

import {
    Banner,
    About,
    Portfolio,
    Contact
} from '../../components'

class Home extends Component {
    render () {
        return (
            <div className="App">
                <div className="container">
                    <Banner />
                    <About />
                    <Portfolio />
                    <Contact />
                </div>
            </div>
        )
    }
}

export default Home