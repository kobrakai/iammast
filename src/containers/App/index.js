import React, {Component} from 'react'
import {Home} from '../'
import {Header, Footer} from '../../components'
import {Switch, Route} from 'react-router-dom'
import './styles.scss'

class App extends Component {
    constructor(props) {
        super(props)

        this.state = {
            hour: new Date().getHours(),
            minHour: 16,
            maxHour: 9
        }
    }

    getClass () {
        if (this.state.hour > this.state.minHour || this.state.hour < this.state.maxHour) {
            return 'app dark';
        } else {
            return 'app light';
        }
    }

    render () {
        return (
            <div className={this.getClass()}>
                <Header />
                <div className="container">
                    <Switch>
                        <Route path="/" component={Home} />
                    </Switch>
                </div>
                <Footer />
            </div>
        )
    }
}

export default App